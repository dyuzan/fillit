/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maastie <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/09 15:49:04 by maastie           #+#    #+#             */
/*   Updated: 2016/06/09 15:49:06 by maastie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H

# include <fcntl.h>
# include <unistd.h>
# include <sys/types.h>
# include <sys/uio.h>
# include "../libft/libft.h"
# define  BUFFER 21

typedef struct 			s_tetriminos
{
	char				*line;
	int					use;
	int					form;
	int					n;
	char				l;
	struct s_tetriminos	*next;

}					t_tetriminos;

typedef struct 			s_content
{
	struct s_tetriminos		*begin;
	struct s_tetriminos		*list;
	char				**finish;
	int					bakctrak;

}					t_content;

t_content			*ft_free_struct(t_content *d);
t_content			*ft_add_to_list(t_content *data, char *tetris);
char				**ft_free_tab(char **tab);
char				**cpy_tab(char **dst, char **src, int k);
int					put_error(int error, int ret);
int					ft_work_on_list(t_content *data);
int					check_tab_tetris(char *str);
int					lblock(char *s, int i);
int					tblock(char *s, int i);
int 				sblock(char *str, int i);
int					add(int f, char *s);

#endif