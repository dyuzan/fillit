/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maastie <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/09 18:40:32 by maastie           #+#    #+#             */
/*   Updated: 2016/06/09 18:40:33 by maastie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fillit.h"

static t_tetriminos	*ft_add_in_list(t_tetriminos *list, char let, int f)
{
	if ((list = (t_tetriminos *)malloc(sizeof(t_tetriminos))) == NULL)
		return (NULL);
	list->use = 0;
	list->n = back;
	list->l = let;
	list->form = f;
	list->next = NULL;
	return (list);
}

t_content	*ft_add_to_list(t_content *data, int f)
{
	static char	letter;
	static int	backtracking;

	if (!data->begin) // initialise le debut d une liste chainee = premier maillon 
	{
		letter = 'A';
		backtracking = 1;
		if ((data->list = (t_tetriminos *)malloc(sizeof(t_tetriminos))) == NULL)
			return (NULL);
		data->begin = data->list;
		data->list->n = backtracking;
		data->list->use = 0;
		data->list->form = f;
		data->list->l = letter;
		data->list->next = NULL;
		return (data);
	}
	backtracking++;
	letter++;
	data->list->next = ft_add_in_list(data->list->next, letter, f, backtracking);
	data->list = data->list->next;
	return (data);
}