/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_tab_tetris_next2.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dyuzan <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/15 22:10:23 by dyuzan            #+#    #+#             */
/*   Updated: 2016/06/15 22:10:25 by dyuzan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fillit.h"

int		check_s_right(char *str, int i)
{
	if (str && str[i + 1] == '#' && str[i + 3] && str[i + 4] == '#')
		return(4);
	else if (str[i + 5] == '#' && str[i + 6] == '#' && str[i + 11] == '#')
		return (5);
	return (-1);
}

int		check_s_left(char *str, int i)
{
	if (str && str[i + 1] == '#' && str[i + 6] == '#' && str[i + 7] == '#')
		return (6);
	else if (str && str[i + 4] == '#' && str[i + 5] == '#' && str[i + 9] == '#')
		return (7);
	return (-1);
}

int		sblock(char *str, int i)
{
	int	ret;

	ret = -1;
	if ((ret = check_s_left(str, i)) > 0)
		return (ret);
	else if ((ret = check_s_right(str, i)) > 0)
		return (ret);
	return (ret);
}