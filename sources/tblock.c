/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tblock.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maastie <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/15 17:07:15 by maastie           #+#    #+#             */
/*   Updated: 2016/06/15 17:07:16 by maastie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fillit.h"

int		tblock(char *s, int i)
{
	if ((s && s[i]) && s[i + 1] && s[i + 1] == '#')
	{
		if ((s && s[i]) && s[i + 2] && s[i + 2] == '#')
			if ((s && s[i]) && s[i + 6] && s[i + 6] == '#')
				return (16);
	}
	else if ((s && s[i]) && s[i + 4] && s[i + 4] == '#')
	{
		if ((s && s[i]) && (s[i + 5] && s[i + 5] == '#'))
		{
			if ((s && s[i]) && (s[i + 6] && s[i + 6] == '#'))
				return (17);
			else if ((s && s[i]) && (s[i + 10] && s[i + 10] == '#'))
				return (18);
		}
	}
	else if ((s && s[i]) && (s[i + 5] && s[i + 5] == '#'))
		if ((s && s[i]) && (s[i + 6] && s[i + 6] == '#'))
			if ((s && s[i]) && (s[i + 10] && s[i + 10] == '#'))
				return (19);
	return (-1);
}