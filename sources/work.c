/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   work.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maastie <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/09 20:58:00 by maastie           #+#    #+#             */
/*   Updated: 2016/06/09 20:58:01 by maastie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fillit.h"

static char	**set_tab(char **tab)
{
	int		i;
	int		l;

	i = 0;
	l = 0;
	if ((tab = (char **)malloc(sizeof(char *) * 53)) == NULL)
		return (NULL);
	while (i < 52)
	{
		tab[i] = NULL;
		if ((tab[i] = (char *)malloc(sizeof(char) * 52)) == NULL)
			return (NULL);
		i++;
	}
	i = 0;
	while (i < 4)
	{
		ft_bzero(tab[i], 52);
		ft_strcpy(tab[i], "....");
		i++;
	}
	// i = 0;
	// while (tab && tab[i])
	// {
	// 	if (tab[i] && tab[i][0] != '\0')
	// 		ft_putendl(tab[i]);
	// 	i++;
	// }
	// ft_putendl("NEW TAB");
	return (tab);
}

char	**bigger(char **s)
{
	int	x;
	int	y;

	x = 0;
	y = 0;
	while (s && s[x][0] != '\0')
	{
		while (s[x] && s[x][y])
			y++;
		s[x][y] = 'y';
		y = 0;
		x++;
	}
	while (s[x - 1][y])
	{
		s[x][y] = 'y';
		y++;
	}
	s[x][y + 1] = 'y';
	return (s);
}

int			ft_work_on_list(t_content *d)
{
	t_tetriminos	*tmp;
	int				l;

	l = 0;
	d->finish = set_tab(d->finish);
	tmp = d->begin;
	d->finish = bigger(d->finish);
	// while (d->finish && d->finish[l])
	// {
	// 	if (d->finish && d->finish[l][0] != '\0')
	// 		ft_putendl(d->finish[l]);
	// 	l++;
	// }
	while (tmp)
	{
//		if (add(tmp->form, d->finish) == 0)
//			tmp->use = 1;
		 ft_putendl("TEST");
		 ft_putchar(tmp->l);
		 ft_putchar('\n');
		 ft_putendl(ft_itoa(tmp->form));
		 ft_putendl("END TEST");
		 tmp = tmp->next;
	}
	return (0);
}