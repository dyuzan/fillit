/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*    init.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dyuzan <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/07 18:24:00 by dyuzan            #+#    #+#             */
/*   Updated: 2016/06/08 18:59:52 by dyuzan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

# include "../includes/fillit.h"

static int	check(char **tab, int d, int j, int i)
{
	while (tab && tab[i])
	{
		j = 0;
		while (tab && tab[i][j])
		{
			if ((tab && tab[i][j]) && (tab[i][j] != '.' || tab[i][j] != '#'))
			{
				if ((tab && tab[i][j]) && tab[i][j] == '#')
					d++;
			}
			else
				return (-1);
			j++;
		}
		if (j != 4)
			return (-1);
		i++;
	}
	if (i != 4)
		return (-1);
	return (0);
}

static int check_tetrimonos(t_content *data, char *tetris)
{
	char		**tab;
	int			ret;

	ret = -1;
	tab = ft_strsplit(tetris, '\n');
	if (check(tab, 0, 0, 0) != 0)
	{
		ft_free_tab(tab);
		return (-1);
	}
	ft_free_tab(tab);
	if ((ret = check_tab_tetris(tetris)) == -1)
		return (put_error(5, -2));
	data = ft_add_to_list(data, ret);
	return (0);
}

static int		ft_open_read(t_content *data, char *file)
{
	int		fd;
	int		ret;
	int		nbt;
	char	buf[BUFFER + 1];

	fd = 0;
	ret = 0;
	nbt = 0;
	if ((fd = open(file, O_RDONLY)) == -1)
		return (put_error(3, -2));
	while ((ret = read(fd, buf, BUFFER)) && nbt <= 26)
	{
		buf[ret] = '\0';
		if (check_tetrimonos(data, buf) == -1 || buf[0] == '\n')
			return (-1);
		else
			nbt++;
	}
	close(fd);
	if (nbt > 26)
		return (put_error(4, -2));
	return (0);
}

int main (int argc, char **argv)
{
	t_content *data;

	if (argc != 2)
		return (put_error(1, -1));
	if ((data = (t_content *)malloc(sizeof(t_content))) == NULL)
		return (-1);
	data->begin = NULL;
	data->list = NULL;
	data->begin = NULL;
	data->finish = NULL;
	if (ft_open_read(data, argv[1]) == -1)
	{
		data = ft_free_struct(data);
		return (put_error(2, -1));
	}
	if (ft_work_on_list(data) == -1)
	{
		data = ft_free_struct(data);
		return (-1);
	}
	data = ft_free_struct(data);
	return (0);
}
