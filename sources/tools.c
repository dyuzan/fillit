/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maastie <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/09 18:42:30 by maastie           #+#    #+#             */
/*   Updated: 2016/06/09 18:42:31 by maastie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fillit.h"

char				**ft_free_tab(char **tab)
{
	int				i;

	i = 0;
	if (tab && !tab[i])
		tab = NULL;
	else
		while (tab && tab[i])
		{
			ft_strdel(&tab[i]);
			i++;
		}
	free(tab);
	return (NULL);
}

char				*tcpy(char *s, char *l)
{
	int				i;

	i = 0;
	while (l && l[i])
	{
		s[i] = l[i];
		i++;
	}
	return (s);
}

char				**cpy_tab(char **dst, char **src, int k)
{
	int				i;
	int				l;

	i = 0;
	l = 0;
	while (src && src[i])
		i++;
	if (i == 0 || (dst = (char **)malloc(sizeof(char *) * (i + k))) == NULL)
		return (NULL);
	while (l != (i + k))
		dst[l++] = NULL;
	i = 0;
	while (src && src[i])
	{
		dst[i] = ft_strdup(src[i]);
		i++;
	}
	return (dst);
}

t_content			*ft_free_struct(t_content *d)
{
	t_tetriminos	*tmp;
	t_tetriminos	*k;

	tmp = d->begin;
	while (tmp)
	{
		k = tmp->next;
		free(tmp);
		tmp = k;
	}
	if (d->finish)
		d->finish = ft_free_tab(d->finish);
	free(d);
	return (NULL);
}