/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_tab_tetris.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dyuzan <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/15 19:17:05 by dyuzan            #+#    #+#             */
/*   Updated: 2016/06/15 19:17:09 by dyuzan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fillit.h"

static int		square(char *s, int i)
{
	if ((s && s[i + 1]) && s[i + 1] == '#')
	{
		if ((s && s[i + 5]) && s[i + 5] == '#')
			if ((s && s[i + 6]) && s[i + 6] == '#')
				return (1);
		return (-1);
	}
	return (-1);
}

static	int		barre(char *s, int i)
{
	if ((s && s[i] == '#') && s[i +5] == '#' && s[i + 10 == '#'] && s[i + 15] == '#')
		return (2);
	else if (s && s[i + 1] == '#' && s[i + 2] == '#' && s[i + 3] == '#')
		return (3);
	return (-1);
}

static int 		check_tetris(char *str, int i)
{
	int			ret;
	if ((ret = square(str, i)) > 0)
		return (ret);
	else if ((ret = barre(str, i)) > 0)
		return (ret);
	else if ((ret = sblock(str, i)) > 0)
		return (ret);
	else if ((ret = lblock(str, i)) > 0)
		return (ret);
	else if ((ret = tblock(str, i)) > 0)
		return (ret);
	return (-1);
}

int				check_tab_tetris(char *str)
{
	int		i;
	int		ret;

	i = 0;
	while (str && str[i] && (str[i] != '#'))
		i++;
	if ((ret = check_tetris(str, i)) == -1)
		return (put_error(6, -2));
	return (ret);
}