/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lblock.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maastie <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/15 17:07:04 by maastie           #+#    #+#             */
/*   Updated: 2016/06/15 17:07:05 by maastie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fillit.h"

static int ft_lleft(char *s, int i)
{
	if ((s && s[i]) && (s[i + 1] && s[i + 1] == '#'))
	{
		if ((s && s[i]) && (s[i + 2] && s[i + 2] == '#'))
		{
			if ((s && s[i]) && (s[i + 7] && s[i + 7] == '#'))
				return (8);
		}
		else if ((s && s[i]) && (s[i + 5] && s[i + 5] == '#'))
			if ((s && s[i]) && (s[i + 10] && s[i + 10] == '#'))
				return (9);
	}
	else if ((s && s[i]) && (s[i + 5] && s[i + 5] == '#'))
	{
		if ((s && s[i]) && (s[i + 6] && s[i + 6] == '#'))
		{
			if ((s && s[i]) && (s[i + 7] && s[i + 7] == '#'))
				return (10);
		}
		else if ((s && s[i]) && (s[i + 9] && s[i + 9] == '#'))
			if ((s && s[i]) && (s[i + 10] && s[i + 10] == '#'))
				return (11);
	}
	return (-1);
}


static int	ft_lright(char *s, int i)
{
	if ((s && s[i]) && (s[i + 1] && s[i + 1] == '#'))
	{
		if ((s && s[i]) && (s[i + 2] && s[i + 2] == '#'))
		{
			if ((s && s[i]) && (s[i + 5] && s[i + 5] == '#'))
				return (12);
		}
		else if ((s && s[i]) && (s[i + 6] && s[i + 6] == '#'))
			if ((s && s[i]) && (s[i + 11] && s[i + 11] == '#'))
				return (13);
	}
	else if ((s && s[i]) && (s[i + 5] && s[i + 5] == '#'))
	{
		if ((s && s[i]) && (s[i + 10] && s[i + 10] == '#'))
		{
			if ((s && s[i]) && (s[i + 11] && s[i + 11] == '#'))
				return (14);
		}
		else if ((s && s[i]) && (s[i + 4] && s[i + 4] == '#'))
			if ((s && s[i]) && (s[i + 3] && s[i + 3] == '#'))
				return (15);
	}
	return (-1);
}

int			lblock(char *s, int i)
{
	int		ret;

	ret = -1;
	if ((ret = ft_lleft(s, i)) > 0)
		return (ret);
	else if ((ret = ft_lright(s, i)) > 0)
		return (ret);
	return (ret);
}