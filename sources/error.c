/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maastie <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/09 18:49:59 by maastie           #+#    #+#             */
/*   Updated: 2016/06/09 18:50:03 by maastie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fillit.h"

int	put_error(int e, int ret)
{
	e == 1 ? ft_putendl_fd("error : Put a VALID file", 2) : e;
	e == 2 ? ft_putendl_fd("error : Invalid file or tetriminos", 2) : e;
	e == 3 ? ft_putendl_fd("error : File not found", 2) : e;
	e == 4 ? ft_putendl_fd("error : more than 26 tetrimonos", 2) : e;
	e == 5 ? ft_putendl_fd("error : not a valid tetriminos", 2) : e;
	return (ret);
}