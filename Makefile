NAME = fillit

HEADER = -I ./includes

SRC = ./sources/init.c\
		./sources/list.c\
		./sources/error.c\
		./sources/tools.c\
		./sources/work.c\
		./sources/tblock.c\
		./sources/lblock.c\
		./sources/block_s.c\
		./sources/check_tab_tetris.c\
		./sources/add.c

CFLAG = -Wall -Werror -Wextra

.PHONY: all clean

all: $(NAME)

$(NAME):
	 make re -C libft
	 clang $(CFLAG) $(SRC) $(HEADER) -o $(NAME) ./libft/libft.a

clean:
	make clean -C libft
	rm -rf $(NAME)

re: clean all

